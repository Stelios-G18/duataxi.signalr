﻿using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Entities
{
    public class Notification : BaseEntity
    {           
        public string PaymentId { get; set; }
        public string CustomerId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }        
        public double DayOfExpiration { get; set; }

        public string Type { get; set; }

        public Notification():base()
        {

        }

    }
}
