﻿using DuaTaxi.ErrorHandlerSignalr.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Repository
{
    public interface INotificationRepository
    {
        Task AddAsync(Notification notification);
        Task UpdateAsync(Notification notification);
        Task DeleteAsync(string Id);
        Task<Notification> GetAsync(string Id);
        Task<IEnumerable<Notification>> FindAsync(string Id); 
        Task<IEnumerable<Notification>> FindByIdAsync(string Id);
        Task<long> CountAsync(string Id);


    }
}
