﻿using DuaTaxi.Common.Mongo;
using DuaTaxi.ErrorHandlerSignalr.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Repository
{
    public class NotificationRepository : INotificationRepository
    {
        IMongoRepository<Notification> _repository;
        public NotificationRepository(IMongoRepository<Notification> repository)
        {
            _repository = repository;
        }

        public async Task AddAsync(Notification notification)
            => await _repository.AddAsync(notification);

        public async Task DeleteAsync(string Id)
            => await _repository.DeleteAsync(Id);

        public async Task<Notification> GetAsync(string Id)
            => await _repository.GetAsync(Id);

        public async Task<long> CountAsync(string Id)
            => await _repository.CountAsync(x=> x.CustomerId ==Id && !x.IsDeleted);

        public async Task<IEnumerable<Notification>> FindAsync(string Id)
            => await _repository.FindAsync(x=> x.CustomerId == Id && !x.IsDeleted);
        public async Task<IEnumerable<Notification>> FindByIdAsync(string Id)
            => await _repository.FindAsync(x => x.Id == Id && !x.IsDeleted);

        public async Task UpdateAsync(Notification notification)
            => await _repository.UpdateAsync(notification);
    }
}
