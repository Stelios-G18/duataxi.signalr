﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Consul;
using DuaTaxi.Common;
using DuaTaxi.Common.Consul;
using DuaTaxi.Common.Dispatchers;
using DuaTaxi.Common.Jaeger;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.Swagger;
using DuaTaxi.Services.Signalr.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DuaTaxi.Services.Signalr.Messages.Events;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Services.Signalr.Framework;
using DuaTaxi.Common.Redis;
using DShop.Common.Authentication;
using DuaTaxi.Services.Signalr.Messages.Events.DeleteAccount;
using DuaTaxi.Services.Signalr.Messages.Events.UpdateUserInfo;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdataTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.DeleteTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdateUserPassword;
using DuaTaxi.Services.Signalr.Services.TimeHostService;
using DuaTaxi.Common.Mongo;
using DuaTaxi.ErrorHandlerSignalr.Entities;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Services.Signalr.Services;
using DuaTaxi.Common.CustomApiCheck;

namespace DuaTaxi.Services.Signalr
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IContainer Container { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCustomMvc();
            services.AddSwaggerDocs();
            services.AddInitializers(typeof(IMongoDbInitializer));
            services.AddConsul();
            services.AddJaeger();
            services.AddRedis();
            services.AddJwt();
            services.AddOpenTracing();
            services.RegisterServiceForwarder<IPaymentService>("payment-service");
            services.AddHostedService<NotificationHostedService>();
            services.AddCors(options => {           
                options.AddPolicy(name: "CorsPolicy",
                             cors => {
                                 cors.WithOrigins(Configuration["Cors"])
                                    .AllowAnyHeader()
                                    .AllowAnyMethod()
                                    .AllowCredentials();
                             });
            });           

            AddSignalR(services);
        

            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(typeof(Startup).Assembly)
                    .AsImplementedInterfaces();
            builder.Populate(services);
            builder.AddDispatchers();
            builder.AddMongo();
            builder.AddCustomerCheck();
            builder.AddMongoRepository<Notification>("Notification");
            builder.AddRabbitMq();

            Container = builder.Build();
       
            return new AutofacServiceProvider(Container);
        }

        private void AddSignalR(IServiceCollection services)
        {
            var options = Configuration.GetOptions<SignalrOptions>("signalr");
            services.AddSingleton(options);
            var builder = services.AddSignalR();
            if (!options.Backplane.Equals("redis", StringComparison.InvariantCultureIgnoreCase)) {
                return;
            }
            var redisOptions = Configuration.GetOptions<RedisOptions>("redis");
            builder.AddRedis(redisOptions.ConnectionString);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            IApplicationLifetime applicationLifetime,
            IConsulClient client, IStartupInitializer startupInitializer)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local") {
                app.UseDeveloperExceptionPage();
            }
         
            app.UseCors("CorsPolicy");
            //app.UseAllForwardedHeaders();
            app.UseAuthentication();
            app.UseServiceId();
            app.UseMvc();                
            //app.UseStaticFiles();
            app.UseSwaggerDocs();
            //app.UseErrorHandler();
          
            app.UseAccessTokenValidator();            
            app.UseSignalR(routes => {
                routes.MapHub<DuaTaxiHub>($"/duataxihub");
            });
          
            app.UseRabbitMq()
                .SubscribeEvent<OperationPending>(@namespace: "operations")
                .SubscribeEvent<OperationCompleted>(@namespace: "operations")
                .SubscribeEvent<OperationRejected>(@namespace: "operations")
                .SubscribeEvent<DeleteAccountCompleted>(@namespace: "authserver")
                .SubscribeEvent<DeleteAccountRejected>(@namespace: "authserver")
                .SubscribeEvent<UpdateUserCompleted>(@namespace: "authserver")
                .SubscribeEvent<UpdateUserRejected>(@namespace: "authserver")
                .SubscribeEvent<UpdateUserPasswordCompeted>(@namespace: "authserver")
                .SubscribeEvent<UpdateUserPasswordRejected>(@namespace: "authserver")
                .SubscribeEvent<UpdateTaxiDriverCompleted>(@namespace: "taxiapi")
                .SubscribeEvent<UpdateTaxiDriverRejected>(@namespace: "taxiapi")
                .SubscribeEvent<DeleteTaxiDriverCompleted>(@namespace: "taxiapi")
                .SubscribeEvent<DeleteTaxiDriverRejected>(@namespace: "taxiapi");

            var consulServiceId = app.UseConsul();
            applicationLifetime.ApplicationStopped.Register(() => {
                client.Agent.ServiceDeregister(consulServiceId);
                Container.Dispose();
            });

            startupInitializer.InitializeAsync();
        }
    }
}
