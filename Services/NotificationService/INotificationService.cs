﻿using DuaTaxi.ErrorHandlerSignalr.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Services.NotificationService
{
    public interface INotificationService
    {
        Task CreateNotification();

        Task<IEnumerable<Notification>> GetNotficationByCustomerId(string customerId);

        Task<long> GetNumberOfNotification(string customerId);

        Task<bool> UpdataNotificationStatus(string customerId);


    }
}
