﻿using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.ErrorHandlerSignalr.Entities;
using DuaTaxi.ErrorHandlerSignalr.Entities.DTO;
using DuaTaxi.ErrorHandlerSignalr.Repository;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Services.NotificationService
{
    public class NotificationService : INotificationService
    {

        private readonly ILogger<NotificationService> _logger;
        protected IOptions<RestEaseOptions> _RestEaseConfig;
        WebApiClient wac;
        private readonly INotificationRepository _notificationRepository;
        private readonly CheckOptions _check;

        public NotificationService(
            IOptions<RestEaseOptions> RestEaseConfig,
            ILogger<NotificationService> logger,
            INotificationRepository notificationRepository,
            CheckOptions check)
        {
            _logger = logger;
            _RestEaseConfig = RestEaseConfig;
            _notificationRepository = notificationRepository;
            _check = check;
        }


        public async Task CreateNotification()
        {
            try {

                wac = new WebApiClient(_RestEaseConfig, "payment-service" , _check.Password);

                var payments = await wac.LoadAsync<IEnumerable<PaymentDTO>>("Payment/Notification");

                foreach (var item in payments) {
                    var notfication = new Notification() {
                        DayOfExpiration = item.DayOfExpiration,
                        PaymentId = item.Id,
                        CustomerId = item.CustomerId,
                        PhoneNumber = item.PhoneNumber,
                        Email = item.Email,
                        Name = item.Name,
                        Type = "Payment"

                    };
                    var existingnotfication = await _notificationRepository.FindAsync(notfication.CustomerId);

                    var updatedValue = existingnotfication.FirstOrDefault();
                    if (updatedValue != null) {
                        updatedValue.IsDeleted = true;
                        updatedValue.DeletedDate = DateTime.Now;

                        await _notificationRepository.UpdateAsync(updatedValue);
                    }
                    await _notificationRepository.AddAsync(notfication);

                }

            } catch (Exception ex) {
                _logger.LogError("CreateNotification exception :", ex.Message);
                throw new Exception();
            }


        }

        public async Task<IEnumerable<Notification>> GetNotficationByCustomerId(string customerId)
        {
           return await _notificationRepository.FindAsync(customerId);
        }

        public async Task<long> GetNumberOfNotification(string customerId)
        {
            var count = await _notificationRepository.CountAsync(customerId);
            return count;
        }

        public async Task<bool> UpdataNotificationStatus(string Id)
        {
            try {       

                var existingnotfication = await _notificationRepository.FindByIdAsync(Id);

                var updatedValue = existingnotfication.FirstOrDefault();
                if (updatedValue != null) {
                    updatedValue.IsDeleted = true;
                    updatedValue.DeletedDate = DateTime.Now;

                    await _notificationRepository.UpdateAsync(updatedValue);
                }

                return true;

            } catch (Exception ex) {
                _logger.LogError("UpdataNotificationStatus exception :", ex.Message);
                return false;
            }

        }
    }
}
