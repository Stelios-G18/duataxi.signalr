using System.Threading.Tasks;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.DeleteTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdataTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdateUserPassword;
using DuaTaxi.Services.Signalr.Messages.Events;
using DuaTaxi.Services.Signalr.Messages.Events.DeleteAccount;
using DuaTaxi.Services.Signalr.Messages.Events.UpdateUserInfo;

namespace DuaTaxi.Services.Signalr.Services
{
    public interface IHubService
    {
        Task PublishOperationPendingAsync(OperationPending @event);
        Task PublishOperationCompletedAsync(OperationCompleted @event);
        Task PublishOperationRejectedAsync(OperationRejected @event);



        Task PublisUpdateUserCompletedAsync(UpdateUserCompleted @event); 
        Task PublishUpdateUserRejectedAsync(UpdateUserRejected @event);

        Task PublisUpdateUserPasswordCompletedAsync(UpdateUserPasswordCompeted @event);
        Task PublishUpdateUserPasswordRejectedAsync(UpdateUserPasswordRejected @event);

        Task PublishDeleteAccountRejectedAsync(DeleteAccountRejected @event);
        Task PublishDeleteAccountCompletedAsync(DeleteAccountCompleted @event);



        Task PublisUpdateTaxiDriverCompletedAsync(UpdateTaxiDriverCompleted @event);
        Task PublishUpdateTaxiDriverRejectedAsync(UpdateTaxiDriverRejected @event);


        Task PublishDeleteTaxiDriverRejectedAsync(DeleteTaxiDriverRejected @event);
        Task PublishDeleteTaxiDriverCompletedAsync(DeleteTaxiDriverCompleted @event);

    }
}