﻿using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.Signalr.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
    public interface IPaymentService
    {
        [AllowAnyStatusCode]
        [Get("/{customerid}")]
        Task<object> GetAsync([Path] string customerid);
    }
}
