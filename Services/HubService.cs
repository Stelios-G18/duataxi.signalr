using System.Threading.Tasks;
using DuaTaxi.Common;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.DeleteTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdataTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdateUserPassword;
using DuaTaxi.Services.Signalr.Messages.Events;
using DuaTaxi.Services.Signalr.Messages.Events.DeleteAccount;
using DuaTaxi.Services.Signalr.Messages.Events.UpdateUserInfo;

namespace DuaTaxi.Services.Signalr.Services
{
    public class HubService : IHubService
    {
        private readonly IHubWrapper _hubContextWrapper;

        public HubService(IHubWrapper hubContextWrapper)
        {
            _hubContextWrapper = hubContextWrapper;
        }


        #region Operation
        public async Task PublishOperationPendingAsync(OperationPending @event)
            => await _hubContextWrapper.PublishToUserAsync(@event.UserId,
                "operation_pending",
                new {
                    id = @event.Id,
                    name = @event.Name,
                    resource = @event.Resource
                }
            );

        public async Task PublishOperationCompletedAsync(OperationCompleted @event)
            => await _hubContextWrapper.PublishToUserAsync(@event.UserId,
                "operation_completed",
                new {
                    id = @event.Id,
                    name = @event.Name,
                    resource = @event.Resource
                }
            );

        public async Task PublishOperationRejectedAsync(OperationRejected @event)
            => await _hubContextWrapper.PublishToUserAsync(@event.UserId,
                "operation_rejected",
                new {
                    id = @event.Id,
                    name = @event.Name,
                    resource = @event.Resource,
                    code = @event.Code,
                    reason = @event.Message
                }
            );
        #endregion

        #region User Account
        public async Task PublisUpdateUserCompletedAsync(UpdateUserCompleted @event)
           => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "UpdateUserCompleted",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        public async Task PublishUpdateUserRejectedAsync(UpdateUserRejected @event)
        => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "UpdateUserRejected",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );


        public async  Task PublishDeleteAccountRejectedAsync(DeleteAccountRejected @event)
        => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "DeleteAccountRejected",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        public async Task PublishDeleteAccountCompletedAsync(DeleteAccountCompleted @event)
            => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "DeleteAccountCompleted",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        #endregion

        #region TaxiDriver
        public async Task PublisUpdateTaxiDriverCompletedAsync(UpdateTaxiDriverCompleted @event)
          => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "UpdateTaxiDriverComplete",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        public async Task PublishUpdateTaxiDriverRejectedAsync(UpdateTaxiDriverRejected @event)
        => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "UpdateTaxiDriverRejecte",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        public async Task PublishDeleteTaxiDriverRejectedAsync(DeleteTaxiDriverRejected @event)
        => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "DeleteTaxiDriverRejecte",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        public async Task PublishDeleteTaxiDriverCompletedAsync(DeleteTaxiDriverCompleted @event)
        => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "DeleteTaxiDriverCompleted",
                new {
                    id = @event.Id,
                    name = @event.Reason,
                    resource = @event.Code
                }
            );

        public async Task PublisUpdateUserPasswordCompletedAsync(UpdateUserPasswordCompeted @event)
          => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "UpdateUserPasswordCompleted",
                new {
                    @event.Id,
                    @event.Reason,
                    @event.Code
                }
            );

        public async Task PublishUpdateUserPasswordRejectedAsync(UpdateUserPasswordRejected @event)
          => await _hubContextWrapper.PublishToUserAsync(@event.Id.ToGuid(),
                "UpdateUserPasswordRejected",
                new {
                    @event.Id,
                    @event.Reason,
                    @event.Code
                }
            );
        #endregion
    }
}