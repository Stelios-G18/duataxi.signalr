﻿
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.ErrorHandlerSignalr.Entities;
using DuaTaxi.ErrorHandlerSignalr.Entities.DTO;
using DuaTaxi.ErrorHandlerSignalr.Repository;
using DuaTaxi.ErrorHandlerSignalr.Services.NotificationService;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DuaTaxi.Services.Signalr.Services.TimeHostService
{
    public class NotificationHostedService : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<NotificationHostedService> _logger;
        private Timer _timer;
        private readonly INotificationService _notificationService;




        public NotificationHostedService(ILogger<NotificationHostedService> logger, INotificationService notificationService)
        {
            _logger = logger;
            _notificationService = notificationService;
        }

        public async Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");
           

            try {

                ////TimeSpan start = new TimeSpan(24,0,0);
                //TimeSpan start = new TimeSpan(0, 0, 30);
                ////TimeSpan now = DateTime.Now.TimeOfDay;
                //while (true) {
                //    await Task.Delay(start, stoppingToken);

                //    await _notificationService.CreateNotification();
                //    var count = Interlocked.Increment(ref executionCount);
                //    _logger.LogInformation(
                //        "Timed Hosted Service is working. CreateNotificationAsync : {Count}", count);


                //}

            } catch (Exception) {

                throw;
            }

        }

        private void  DoWork(object state)
        {                         
          




        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
