#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.
#  cd ..     docker build -f .\DuaTaxi.Operations\Dockerfile -t stelgio/duataxi_errorhandler:v2 .
#            docker run -p 5007:5007 --network=duataxi-network  stelgio/duataxi_errorhandler:v2  
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine  AS base
WORKDIR /app
ENV ASPNETCORE_URLS http://*:5007
ENV ASPNETCORE_ENVIRONMENT docker
EXPOSE 5007

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build
WORKDIR /src
COPY ["DuaTaxi.ErrorHandlerSignalr/DuaTaxi.ErrorHandlerSignalr.csproj", "DuaTaxi.ErrorHandlerSignalr/"]
RUN dotnet restore "DuaTaxi.ErrorHandlerSignalr/DuaTaxi.ErrorHandlerSignalr.csproj"
COPY . .
WORKDIR "/src/DuaTaxi.ErrorHandlerSignalr"
RUN dotnet build "DuaTaxi.ErrorHandlerSignalr.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DuaTaxi.ErrorHandlerSignalr.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DuaTaxi.ErrorHandlerSignalr.dll"]