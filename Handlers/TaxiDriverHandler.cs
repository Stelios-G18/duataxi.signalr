﻿using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.DeleteTaxiDriver;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdataTaxiDriver;
using DuaTaxi.Services.Signalr.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Handlers
{
    public class TaxiDriverHandler :
        IEventHandler<DeleteTaxiDriverRejected>, IEventHandler<DeleteTaxiDriverCompleted>

    {
        private readonly IHubService _hubService;

        public TaxiDriverHandler(IHubService hubService)
        {
            _hubService = hubService;
        }

        public async Task HandleAsync(DeleteTaxiDriverRejected @event, ICorrelationContext context)
          => await _hubService.PublishDeleteTaxiDriverRejectedAsync(@event);

        public async Task HandleAsync(DeleteTaxiDriverCompleted @event, ICorrelationContext context)
           => await _hubService.PublishDeleteTaxiDriverCompletedAsync(@event);

     
    }
}
