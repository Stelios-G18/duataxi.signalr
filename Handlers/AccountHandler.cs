using System.Threading.Tasks;
using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdataTaxiDriver;
using DuaTaxi.Services.Signalr.Messages.Events;
using DuaTaxi.Services.Signalr.Messages.Events.DeleteAccount;
using DuaTaxi.Services.Signalr.Messages.Events.UpdateUserInfo;
using DuaTaxi.Services.Signalr.Services;

namespace DuaTaxi.Services.Signalr.Handlers
{
    public class AccountHandler :
        IEventHandler<DeleteAccountCompleted>,IEventHandler<DeleteAccountRejected>,
        IEventHandler<UpdateUserCompleted>,IEventHandler<UpdateUserRejected> ,
                IEventHandler<UpdateTaxiDriverCompleted>, IEventHandler<UpdateTaxiDriverRejected>
    {
        private readonly IHubService _hubService;
        
        public AccountHandler(IHubService hubService)
        {
            _hubService = hubService;
        }

        public async Task HandleAsync(UpdateUserCompleted @event, ICorrelationContext context)        
             => await _hubService.PublisUpdateUserCompletedAsync(@event);

        public async Task HandleAsync(DeleteAccountRejected @event, ICorrelationContext context)
            => await _hubService.PublishDeleteAccountRejectedAsync(@event);

        public async Task HandleAsync(DeleteAccountCompleted @event, ICorrelationContext context)
           => await _hubService.PublishDeleteAccountCompletedAsync(@event);

        public async Task HandleAsync(UpdateUserRejected @event, ICorrelationContext context)
           => await _hubService.PublishUpdateUserRejectedAsync(@event);

        public async Task HandleAsync(UpdateTaxiDriverCompleted @event, ICorrelationContext context)
          => await _hubService.PublisUpdateTaxiDriverCompletedAsync(@event);

        public async Task HandleAsync(UpdateTaxiDriverRejected @event, ICorrelationContext context)
          => await _hubService.PublishUpdateTaxiDriverRejectedAsync(@event);
    }
}