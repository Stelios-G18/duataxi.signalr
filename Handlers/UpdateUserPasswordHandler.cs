﻿using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.ErrorHandlerSignalr.Messages.Events.UpdateUserPassword;
using DuaTaxi.Services.Signalr.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.ErrorHandlerSignalr.Handlers
{
    public class UpdateUserPasswordHandler : IEventHandler<UpdateUserPasswordCompeted>, IEventHandler<UpdateUserPasswordRejected>
    {
        private readonly IHubService _hubService;

        public UpdateUserPasswordHandler(IHubService hubService)
        {
            _hubService = hubService;
        }      

        public async Task HandleAsync(UpdateUserPasswordCompeted @event, ICorrelationContext context)
          => await _hubService.PublisUpdateUserPasswordCompletedAsync(@event);

        public async Task HandleAsync(UpdateUserPasswordRejected @event, ICorrelationContext context)
           => await _hubService.PublishUpdateUserPasswordRejectedAsync(@event);
    }
}
