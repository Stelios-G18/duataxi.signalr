﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.ErrorHandlerSignalr.Entities;
using DuaTaxi.ErrorHandlerSignalr.Services.NotificationService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DuaTaxi.ErrorHandlerSignalr.Controllers
{
    [Route("api/[controller]")]
    [ServiceFilter(typeof(AuthValidation))]
    public class NotificationController : ControllerBase
    {
       
        private ILogger<NotificationController> _logger;

        private readonly INotificationService _notificationService;

        public NotificationController(ILogger<NotificationController> _logger , INotificationService notificationService) 
        {
            this._logger = _logger;
            _notificationService = notificationService;
           
        }


        [Route("Count/{CustomerId}")]
        [HttpGet]
        public async Task<SrvResp<long>> Count(string CustomerId)
        {
            try {

                var number = await _notificationService.GetNumberOfNotification(CustomerId);
                return new SrvResp<long>(number);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<long>(ex);
            }

        }

        [Route("Data/{CustomerId}")]
        [HttpGet]
        public async Task<SrvResp<IEnumerable<Notification>>> Data(string CustomerId)
        {
            try {
                var notifications = await _notificationService.GetNotficationByCustomerId(CustomerId);
                return new SrvResp<IEnumerable<Notification>>(notifications);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<IEnumerable<Notification>>(ex);
            }

        }

        [Route("Update/{CustomerId}")]
        [HttpGet]
        public async Task<SrvResp<bool>> Update(string CustomerId)
        {
            try {
                var status = await _notificationService.UpdataNotificationStatus(CustomerId);

                return new SrvResp<bool>(status);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<bool>( ex);
            }

        }



    }
}