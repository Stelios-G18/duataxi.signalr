using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using DuaTaxi.Services.Signalr.Framework;
using DShop.Common.Authentication;

namespace DuaTaxi.Services.Signalr.Hubs
{
    public class DuaTaxiHub : Hub
    {
        private readonly IJwtHandler _jwtHandler;

        public DuaTaxiHub(IJwtHandler jwtHandler)
        {
            _jwtHandler = jwtHandler;
        }

        public async Task InitializeAsync(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                await DisconnectAsync();
            }
            try
            {
                //var payload = _jwtHandler.GetTokenPayload(token);
                if (token == null)
                {
                    await DisconnectAsync();
                    
                    return;
                }
                var group = Guid.Parse(token).ToUserGroup();
                await Groups.AddToGroupAsync(Context.ConnectionId, group);
                await ConnectAsync();
            }
            catch
            {
                await DisconnectAsync();
            }
        }

        private async Task ConnectAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("connected");
 
        }

        private async Task DisconnectAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("disconnected");
        }
    }
}